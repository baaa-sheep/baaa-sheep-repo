﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Analytics;
using FMODUnity;

public class GameManager : NetworkBehaviour
{
    public static GameManager TheGameManager;
    [SyncVar]
    public int NumberOfPlayers;

    [SyncVar]
    public bool IsGameOn;

    [SyncVar]
    public bool IsGameOver;

    public float TimerPerRound;
    [SyncVar]
    public float TimeRemaining;

    public Text TimeRemaingText;
    public Text GameOverText;
    public Text TotalPlayersText;
    public Text RemainingHidersText;

    public List<GameObject> PreSpawnPlayers = new List<GameObject>();
    public List<GameObject> SpawnedPlayers = new List<GameObject>();

    public List<GameObject> Collectables = new List<GameObject>();
    int Collected;
    public List<GameObject> Hiders = new List<GameObject>();
    [SyncVar]
    int HidersDead;
    [SyncVar]
    int RemainingHiders;
    [SyncVar]
    string GameOverTextMessege;

    public GameObject LoobyBackgroundImage;
    public GameObject DeadMessage;
    public GameObject StartButton;
    public GameObject LightButton;
    public Text LightText;
    [SyncVar]
    public bool LightIsAlwaysOn;

    public GameObject GlobalLight;

    private void Awake()
    {
        // Set singleton
        TheGameManager = this;

        // Default light mode
        LightIsAlwaysOn = false;

    }

    void Start()
    {
        // Set time when first player join
        if (NumberOfPlayers <= 1)
        {
            TimeRemaining = TimerPerRound;
            TimeRemaingText.text = TimeRemaining.ToString();
        }
        // disable lobby buttons for non-server, currently works in build only (not editor)
        if (NumberOfPlayers > 1)
        {
            StartButton.SetActive(false);
            LightButton.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //count active players and display it
        if (isServer)
        {
            CountPlayers();
        }

        // If the game started...
        if (IsGameOn)
        {
            // countdown time
            if (isServer)
            {
                TimeRemaining = TimeRemaining - Time.deltaTime;
                // Check for game end conditions
                CheckforWin();
            }
            // Disable Lobby buttons and background, update timer
            
            TimeRemaingText.text = Mathf.RoundToInt(TimeRemaining).ToString();
            LoobyBackgroundImage.SetActive(false);
            StartButton.SetActive(false);
            LightButton.SetActive(false);
        }
        else
        {
            LoobyBackgroundImage.SetActive(true);
        }

        TotalPlayersText.text = "Number of Players:  " + NumberOfPlayers.ToString();
        // Display remaining hiders
        RemainingHidersText.text = "Remaining Hiders:  " + RemainingHiders.ToString();
        if (IsGameOver)
        {
            GameOverText.text = GameOverTextMessege;
        }
    }

    void CheckforWin()
    {
        //  Check if time expired
        if (TimeRemaining <= 0)
        {
            GameOverTextMessege = "Time is up";

            AnalyticsEvent.Custom("Game Ends", new Dictionary<string, object>
             {
                 { "Time is up", "Time is up" },
             });

            RpcEndGame();
        }

        // Check if all collectables were collected
        Collected = 0;
        foreach (GameObject CollectObject in Collectables)
        {
            if (CollectObject.GetComponent<Collectable>().IsCollected)
            {
                Collected++;
            }
        }
        if (Collectables.Count <= Collected)
        {
            GameOverTextMessege = "Hiders Win - Collection";

            AnalyticsEvent.Custom("Game Ends", new Dictionary<string, object>
             {
                 { "Hiders Win", "Hiders Win" },
             });

            RpcEndGame();
        }

        // Check if all Hiders are dead
        HidersDead = 0;
        RemainingHiders = 0;
        Hiders.RemoveAll(item => item == null);

        // Cound dead hiders and remaining hiders
        foreach (GameObject Hider in Hiders)
        {
            if (Hider.GetComponent<Hider>().IsDead)
            {
                HidersDead++;
            }
            else
            {
                RemainingHiders++;
            }
        }


        // If all hiders are dead end game
        if (Hiders.Count <= HidersDead)
        {
            GameOverTextMessege = "Chaser Wins - Enemies Dead";

            AnalyticsEvent.Custom("Game Ends", new Dictionary<string, object>
             {
                 { "Chasers Win", "Chasers Win" },
             });

            RpcEndGame();
        }
    }

    void CountPlayers()
    {
        if (IsGameOn|| IsGameOver)
        {
            // if the game started or ended, count spawned players 
            SpawnedPlayers.RemoveAll(item => item == null);
            NumberOfPlayers = SpawnedPlayers.Count;
        }
        else
        {
            // If the game didn't start yet, count preSpawned Players
            PreSpawnPlayers.RemoveAll(item => item == null);
            NumberOfPlayers = PreSpawnPlayers.Count;
        }

    }
    
    // This function is used to spawn players but doesn't start the game right away
    // The game starts after the players spawns to avoid a bug that caused the game to end right away before players even spawn
    public void PrepareGameStart()
    {
        // disable buttons
        StartButton.SetActive(false);
        LightButton.SetActive(false);
        // Spawn players with roles
        for (int i = 0; i < PreSpawnPlayers.Count; i++)
        {
            // Player 1 and player 5  will spawn as chasers for balance
            if (i == 0 || i == 4)
            {
                PreSpawnPlayers[i].GetComponent<PlayerPreSpawn>().isChaser = true;
            }
            else
            {
                PreSpawnPlayers[i].GetComponent<PlayerPreSpawn>().isChaser = false;
            }
            PreSpawnPlayers[i].GetComponent<PlayerPreSpawn>().RpcPrepareToSpawn();
        }
        // Call game start function after delat
        Invoke("RpcStartGame", 1);

        GetComponent<StudioEventEmitter>().SetParameter("GameState", 1);

        AnalyticsEvent.Custom("Lobby Size", new Dictionary<string, object>
             {
                 { "Lobby Size", NumberOfPlayers },
             });
    }

    // This function start the game and and call a function to remove light after delay
    [ClientRpc]
    public void RpcStartGame()
    {
        IsGameOn = true;
        // Lights aren't diables for Light on mode
        if (!LightIsAlwaysOn)
        {
            Invoke("DisableGlobalLight", 3);
        }
    }

    // This function is called when the game ends, it handles UI and background
    [ClientRpc]
    void RpcEndGame()
    {
        LoobyBackgroundImage.SetActive(true);
        IsGameOver = true;
        IsGameOn = false;

        // Send analytics
        AnalyticsEvent.Custom("Mushrooms Collected", new Dictionary<string, object>
             {
                 { "Collected Mushrooms", Collected },
             });

        AnalyticsEvent.Custom("Time Remaining", new Dictionary<string, object>
             {
                 { "Time Remaining", TimeRemaining },
             });
        AnalyticsEvent.Custom("Hiders Survived", new Dictionary<string, object>
             {
                 { "Hiders Survived", RemainingHiders },
             });
    }
    // This handles UI text change for light mode button
    public void SwitchLightMode()
    {
        LightIsAlwaysOn = !LightIsAlwaysOn;

        if (LightIsAlwaysOn)
        {
            LightText.text = "Test Mode: Light On";
        }
        else
        {
            LightText.text = "Default Mode: Dark";
        }
    }

    // This function removes early game light
    // This is done by teleporting the light away, the light position is synced so it is moved on all clients because disabling the light didnt always work on all clients
    void DisableGlobalLight() 
    {
        GlobalLight.transform.position = new Vector3(-222, -222, -222);
    //   GlobalLight.Get
        //GlobalLight.SetActive(false); 
    }

}
