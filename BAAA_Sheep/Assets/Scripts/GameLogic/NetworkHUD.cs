﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking
{
    /// <summary>
    /// An extension for the NetworkManager that displays a default HUD for controlling the network state of the game.
    /// <para>This component also shows useful internal state for the networking system in the inspector window of the editor. It allows users to view connections, networked objects, message handlers, and packet statistics. This information can be helpful when debugging networked games.</para>
    /// </summary>A
    [AddComponentMenu("Network/NetworkManagerHUD")]
    [RequireComponent(typeof(NetworkManager))]
    [EditorBrowsable(EditorBrowsableState.Never)]
    [Obsolete("The high level API classes are deprecated and will be removed in the future.")]
    public class NetworkHUD : MonoBehaviour
    {
        /// <summary>
        /// The NetworkManager associated with this HUD.
        /// </summary>
        public NetworkManager manager;
        /// <summary>
        /// Whether to show the default control HUD at runtime.
        /// </summary>
        [SerializeField] public bool showGUI = true;
        /// <summary>
        /// The horizontal offset in pixels to draw the HUD runtime GUI at.
        /// </summary>
        [SerializeField] public int offsetX;
        /// <summary>
        /// The vertical offset in pixels to draw the HUD runtime GUI at.
        /// </summary>
        [SerializeField] public int offsetY;

        // Runtime variable
        bool m_ShowServer;

        public GUISkin UI_Skin;
        public Texture buttonArt;
        GUIStyle UI_Style;

        void Awake()
        {
            manager = GetComponent<NetworkManager>();
            UI_Style = UI_Skin.GetStyle("button");
            
 
            
        }

        void Update()
        {
            // Checkbox Debug
            if (!showGUI)
                return;
            // KeyBinds commands
            if (!manager.IsClientConnected() && !NetworkServer.active && manager.matchMaker == null)
            {
                if (UnityEngine.Application.platform != RuntimePlatform.WebGLPlayer)
                {
                    if (Input.GetKeyDown(KeyCode.S))
                    {
                        manager.StartServer();
                    }
                    if (Input.GetKeyDown(KeyCode.H))
                    {
                        manager.StartHost();
                    }
                }
                if (Input.GetKeyDown(KeyCode.C))
                {
                    manager.StartClient();
                }
            }
            if (NetworkServer.active)
            {
                if (manager.IsClientConnected())
                {
                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        //manager.StopHost();
                    }
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.X))
                    {
                        //manager.StopServer();
                    }
                }
            }
        }

        void OnGUI()
        {
            //GUI.skin = UI_Skin;
            //GUILayout.Button(buttonArt);
            // Debug Checkbox
            if (!showGUI)
                return;
            // Assign Menu position
            //int xpos = 10 + offsetX;
            //int ypos = 40 + offsetY;
            // Menue will always spawn in the middle 
            int xpos = ((Screen.width)) / 2;
            int ypos = ((Screen.height) / 6*2);
            const int spacing = 85;

            bool noConnection = (manager.client == null || manager.client.connection == null ||
                manager.client.connection.connectionId == -1);
  
            // Show ready button
            if (manager.IsClientConnected() && !ClientScene.ready)
            {
                if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Client Ready"))
                {
                    ClientScene.Ready(manager.client.connection);

                    if (ClientScene.localPlayers.Count == 0)
                    {
                        ClientScene.AddPlayer(0);
                    }
                }
                ypos += spacing;
            }
            // Show quit game button
            if (NetworkServer.active || manager.IsClientConnected())
            {
                if (GUI.Button(new Rect(50,10, 200, 100), "Quit Game", UI_Style))
                {
                    manager.StopHost();
                }
                ypos += spacing;
            }

            if (!NetworkServer.active && !manager.IsClientConnected() && noConnection)
            {
                ypos += 10;

 
                // Check if the player is connected to a matchmaking server
                if (manager.matchMaker == null)
                {
                    // Show start Matchmaking Button

                   if( GUI.Button(new Rect(xpos, ypos + ((Screen.height) / 6 * 2), 200, 100), "Start Game" , UI_Style))
                   // if (GUI.Button(new Rect(xpos, ypos, 200, 20), "Start Matchmaking", UI_Style))
                    {
                        manager.StartMatchMaker();
                    }
                    ypos += spacing;
                }
                else
                {
                    
                    if (manager.matchInfo == null)
                    {
                        if (manager.matches == null)
                        {
                            // Show Create game match
                            if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Create Game", UI_Style))
                            {
                                manager.matchMaker.CreateMatch(manager.matchName, manager.matchSize, true, "", "", "", 0, 0, manager.OnMatchCreate);
                            }
                            ypos += spacing;
                            // Show Room Name label
                            GUI.Label(new Rect(xpos, ypos, 200, 50), "Room Name:", UI_Style);
                            // Show match name input field and assign its value as the match name
                            manager.matchName = GUI.TextField(new Rect(xpos, ypos +50, 200, 50), manager.matchName);
                            ypos += spacing;

                            ypos += 10;
                            // Show Find Match Button
                            if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Find Game", UI_Style))
                            {
                                manager.matchMaker.ListMatches(0, 20, "", false, 0, 0, manager.OnMatchList);
                            }
                            ypos += spacing;
                        }
                        else
                        {
                            // List buttons to allow joing available internet match
                            for (int i = 0; i < manager.matches.Count; i++)
                            {
                                var match = manager.matches[i];
                                if (GUI.Button(new Rect(xpos, ypos - ((Screen.height) / 6 * 2), 200, 100), "Join:" + match.name, UI_Style))
                                {
                                    manager.matchName = match.name;
                                    manager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, manager.OnMatchJoined);
                                }
                                ypos += spacing;
                            }
                            // Show back button
                            if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Back to Search", UI_Style))
                            {
                                manager.matches = null;
                            }
                            ypos += spacing;
                        }
                    }
                    // Show server info
                    if (m_ShowServer)
                    {
                        ypos += spacing;
                        if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Local", UI_Style))
                        {
                            manager.SetMatchHost("localhost", 1337, false);
                            m_ShowServer = false;
                        }
                        ypos += spacing;
                        if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Internet", UI_Style))
                        {
                            manager.SetMatchHost("mm.unet.unity3d.com", 443, true);
                            m_ShowServer = false;
                        }
                        ypos += spacing;
                        if (GUI.Button(new Rect(xpos, ypos, 200, 100), "Staging", UI_Style))
                        {
                            manager.SetMatchHost("staging-mm.unet.unity3d.com", 443, true);
                            m_ShowServer = false;
                        }
                    }

                }
            }
        }
    }
}
