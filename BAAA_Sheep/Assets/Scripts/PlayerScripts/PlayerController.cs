﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class PlayerController : NetworkBehaviour
{



    //CharacterController m_CharacterControl;
    Rigidbody RB;

    [SerializeField] private Animator m_animator = null;

    public float baseSpeed;
    public float rotationSpeed;


    public bool isGrounded;
    [SyncVar]
    public Vector2 InputVector;

    public GameObject Pointer;

    private void Awake()
    {
        isGrounded = true;
        RB = GetComponent<Rigidbody>();

    }

    public virtual void Start()
    {
        // Register Player
        GameManager.TheGameManager.SpawnedPlayers.Add(this.gameObject);
        // activate pointer to show where the player spawned
        if (isLocalPlayer)
        {
            //GameObject NewPointer = Instantiate(Pointer, transform.position, Quaternion.identity);
            //Destroy(NewPointer, 4);
            Pointer.SetActive(true);
            Invoke("DisablePointer", 3);
        }
    }

    void FixedUpdate()
    {
      
        // This bool is required by the asset store package animator used, it tends to bug out and go out of sync, currently we reset it every fixed update
        m_animator.SetBool("Grounded", true);
        m_animator.SetFloat("MoveSpeed", new Vector2(InputVector.x, InputVector.y).magnitude);
        // If this client owns this prefab, get direction from input
       if (isLocalPlayer)
       {
            Move();
       }

      
    }


    void Move()
    {

        //Assign controls to a vector
        InputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
        // Move character
        //RB.velocity = (transform.forward.normalized * baseSpeed * Input.GetAxis("Vertical"));
        // Rotate Character
        //RB.angularVelocity = (Vector3.up * baseSpeed * Input.GetAxis("Horizontal"));
        //RB.velocity = (transform.forward.normalized * baseSpeed * Input.GetAxis("Vertical"));
        
        Vector3 InputVector3D = new Vector3(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical")).normalized;

        if (InputVector3D != Vector3.zero)
        {
            transform.forward = InputVector3D;
            RB.velocity = (InputVector3D * baseSpeed);
        }


    }
    void DisablePointer()
    {
        // View Pointer
        Pointer.SetActive(false);
    }
}
