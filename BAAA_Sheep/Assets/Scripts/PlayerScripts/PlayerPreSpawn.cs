﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// This script is attached to the lobby player prefab (before a role is selected)
public class PlayerPreSpawn : NetworkBehaviour
{
    public GameObject ChaserPrefab;
    public GameObject HiderPrefab;
    GameObject RolePrefab;

    public bool isChaser;

    // Start is called before the first frame update
    void Start()
    {
        // Inform the game object about this player and add it to a list
        if (isServer)
        {
            GameManager.TheGameManager.NumberOfPlayers++;
            GameManager.TheGameManager.PreSpawnPlayers.Add(this.gameObject);
        }


    }

    // Update is called once per frame
    void Update()
    {

    }

    [Command]
    void CmdSpawnPlayerPrefab()
    {
        // Spawn player on all clients
        GameObject NewRolePrefab = Instantiate(RolePrefab, this.transform.position, Quaternion.identity);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, NewRolePrefab, playerControllerId);
    }

    void SetRoleInitial()
    {
        // This function switchs the prefabs that will be spawned based on the assigned role 
        if (isChaser)
        {
            RolePrefab = ChaserPrefab;
        }
        else
        {
            RolePrefab = HiderPrefab;
        }
    }
    [ClientRpc]
    public void RpcPrepareToSpawn()
    {
        // Choose the prefab based on role then spawn it
        SetRoleInitial();
        CmdSpawnPlayerPrefab();
    }
}
