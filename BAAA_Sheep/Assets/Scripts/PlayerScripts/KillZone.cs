﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// This script is placed on a child with a collider on the chaser's prefab, to allow chasers to defeat hiders on collision
public class KillZone : NetworkBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider col)
    {
        print(col.gameObject.name);

        
        // Call death (Die) function on a hider when colliding with it
        if (!System.Object.ReferenceEquals(col.gameObject.GetComponent<Hider>(), null))
        {
            col.gameObject.GetComponent<Hider>().Die();
            print("Die");
        }


    }
}
