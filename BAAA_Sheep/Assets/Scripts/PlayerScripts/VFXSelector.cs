﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class VFXSelector : MonoBehaviour
{
    enum TerrainType
    {
        Grass, Ground, Gravel, Leaves, Water
    }
    public GameObject[] VFXObjects;

    [SerializeField]
    TerrainType m_terrain;


    public PlayerController playerController;



    private void Awake()
    {
        playerController = GetComponentInParent<PlayerController>();
    }

    private void Update()
    {
        CheckTerrain();
        
        // Enable VFX if player is moving
        if (playerController.InputVector.magnitude > 0.01f)
        {
            CmdEnableVFX();
        }
        else
        {
            CmdDisableVFX();
        }
    }

    void CheckTerrain()
    {
        // Use raycast to determine the terrian the player is walking on
        RaycastHit[] hit;

        hit = Physics.RaycastAll(transform.position, Vector3.down, 10.0f);

        foreach (RaycastHit rayhit in hit)
        {
            if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Grass"))
            {
                m_terrain = TerrainType.Grass;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                m_terrain = TerrainType.Ground;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Gravel"))
            {
                m_terrain = TerrainType.Gravel;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Leaves"))
            {
                m_terrain = TerrainType.Leaves;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Water"))
            {
                m_terrain = TerrainType.Water;
            }
        }

    }

    [Command]
    public void CmdEnableVFX()
    {
        RpcEnableVFX();
    }

    [ClientRpc]
    public void RpcEnableVFX()
    {
        // select sound based on terrian
        switch (m_terrain)
        {
            case TerrainType.Grass:
               // DisableVFX();
                VFXObjects[0].SetActive(true);
           
                break;
            case TerrainType.Ground:
              //  DisableVFX();
                VFXObjects[1].SetActive(true);
                break;
            case TerrainType.Gravel:
              //  DisableVFX();
                VFXObjects[2].SetActive(true);
                break;
            case TerrainType.Leaves:
              //  DisableVFX();
                VFXObjects[3].SetActive(true);
                break;
            case TerrainType.Water:
              //  DisableVFX();
                VFXObjects[4].SetActive(true);
                break;
            default:
             //   DisableVFX();
                VFXObjects[1].SetActive(true);
                break;
        }
    }


    [Command]
    public void CmdDisableVFX()
    {
        RpcDisableVFX();
    }

    [ClientRpc]
    // Disable VFX objects function
    public void RpcDisableVFX()
    {
        foreach(GameObject VFXObject in VFXObjects)
        {
            VFXObject.SetActive(false);
        }
    }
}
