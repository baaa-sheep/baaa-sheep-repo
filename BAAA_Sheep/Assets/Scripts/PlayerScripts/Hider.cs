﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Hider : PlayerController
{

    [SyncVar]
    public GameObject Grave;
    [SyncVar]
    public bool IsDead = false;
    [SyncVar]
    Vector3 DeathLocation;
    public SkinnedMeshRenderer AvatarMesh;



    public override void Start()
    {
        base.Start();
        // add this game object to a a list of hiders in the game manager for tracking
        GameManager.TheGameManager.Hiders.Add(this.gameObject);
    }

    void Update()
    {

    }

    // This function teleports the player and call a function to replace the avatar's prefab
    // The Hider game objects isn't destroyed for the sake of game manager tracking
    public void Die()
    {
        IsDead = true;
        DeathLocation = transform.position;
        if (isLocalPlayer)
        {
            transform.position = new Vector3(-200, -222, -222);
            GetComponent<Rigidbody>().MovePosition(new Vector3(-200, -222, -222));
            CmdSpawnGrave();

            if (isLocalPlayer)
            {
                GameManager.TheGameManager.DeadMessage.SetActive(true);
            }
        }
   




    }

    // This function replaces the player avatar with a its dieing/captured prefab for aesthetic reasons
    [Command]
    void CmdSpawnGrave()
    {
        GetComponent<PlayerAudio>().RpcPlayDeath();
        GetComponent<PlayerAudio>().CmdIntializeDeath();
        GameObject NewRolePrefab = Instantiate(Grave, DeathLocation, Quaternion.identity);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, NewRolePrefab, playerControllerId);
    }

}
