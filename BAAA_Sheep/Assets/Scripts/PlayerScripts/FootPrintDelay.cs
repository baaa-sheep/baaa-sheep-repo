﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootPrintDelay : MonoBehaviour
{
    [SerializeField] GameObject footPrint = null;
    [SerializeField] float delayTime = 0;

    private void Start()
    {
        footPrint.SetActive(false);
        StartCoroutine(DealyThisFootPrint());
    }

    IEnumerator DealyThisFootPrint()
    {
        yield return new WaitForSeconds(delayTime);
        footPrint.gameObject.SetActive(true);
    }
}
