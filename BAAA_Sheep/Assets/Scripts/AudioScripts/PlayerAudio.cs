﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using FMODUnity;

public class PlayerAudio : NetworkBehaviour
{

    enum TerrainType
    {
        Grass, Ground, Gravel, Leaves, Water
    }


    [SerializeField]
    TerrainType m_terrain;

    FMOD.Studio.EventInstance footsteps;
    FMOD.Studio.EventInstance death;

    [SerializeField]
    float footstepSpeed;

    private PlayerController playerController;

    float timer = 0.0f;

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        CheckTerrain();
   
        // Check player input to see if they are moving
        if (playerController.InputVector.magnitude > 0.01f)
        {
            if (GetComponent<Hider>())
            {
                // only play sound if hider isn't dead
                if (!GetComponent<Hider>().IsDead)
                {
                    if (timer > footstepSpeed)
                    {
                        PickAndPlayFootstep();

                        timer = 0.0f;
                    }
                }
            }
            // Play sound and reset time counter
            else if (timer > footstepSpeed)
            {
                PickAndPlayFootstep();
 
                timer = 0.0f;
            }
        }
        // Increment time counter
        timer += Time.deltaTime;
    }

    void CheckTerrain()
    {
        // Use raycast to determine the terrian the player is walking on
        RaycastHit[] hit;
   
        hit = Physics.RaycastAll(transform.position, Vector3.down, 10.0f);

              foreach (RaycastHit rayhit in hit)
        {
            if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Grass"))
            {
                m_terrain = TerrainType.Grass;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                m_terrain = TerrainType.Ground;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Gravel"))
            {
                m_terrain = TerrainType.Gravel;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Leaves"))
            {
                m_terrain = TerrainType.Leaves;
            }
            else if (rayhit.transform.gameObject.layer == LayerMask.NameToLayer("Water"))
            {
                m_terrain = TerrainType.Water;
            }
        }

    }

    public void PickAndPlayFootstep()
    {
        // select sound based on terrian
        switch (m_terrain)
        {
            case TerrainType.Grass:
                CmdInitializeFootSteps(1);
                break;
            case TerrainType.Ground:
                CmdInitializeFootSteps(0);
                break;
            case TerrainType.Gravel:
                CmdInitializeFootSteps(2);
                break;
            case TerrainType.Leaves:
                CmdInitializeFootSteps(3);
                break;
            case TerrainType.Water:
                CmdInitializeFootSteps(4);
                break;
            default:
                CmdInitializeFootSteps(0);
                break;
        }

    }


    ///[Command]
    void CmdInitializeFootSteps(int terrain)
    {
        RpcPlayFootstep(terrain);
    }

    [Command]
    public void CmdIntializeDeath()
    {
        RpcPlayDeath();
    }

    ///[ClientRpc]
    void RpcPlayFootstep(int terrain)
    {
        footsteps = FMODUnity.RuntimeManager.CreateInstance("event:/Footsteps");
        footsteps.setParameterByName("Terrain", terrain);
        footsteps.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        footsteps.start();
        footsteps.release();
    }

    [ClientRpc]
    public void RpcPlayDeath() 
    {
        death = FMODUnity.RuntimeManager.CreateInstance("event:/Death");
        death.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        death.start();
        death.release();
    }
}

