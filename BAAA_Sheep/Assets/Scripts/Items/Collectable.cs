﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Collectable : NetworkBehaviour
{
    [SyncVar]
    public bool IsCollected = false;
    // Start is called before the first frame update
    void Start()
    {
        // Register this collectable to a list in the game manager list
        GameManager.TheGameManager.Collectables.Add(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider col)
    {
        // Debug collision check
        print("Collect");
        

        // The collection check is done on sever only
        if (!isServer)
        {
            return;
        }
        
        // Check if it collided with something/someone who can collects
        if (col.gameObject.tag == "Collector")
        {
            // The game manager tracks how many collecatbles in the list are collected and needs to mantain connection to all collectables spawned even if collected
            // Change the bool to indicate it is collected and teleport the object out of the map
            IsCollected = true;
            transform.position = new Vector3(-200,-200,-200);
        }


    }
}
