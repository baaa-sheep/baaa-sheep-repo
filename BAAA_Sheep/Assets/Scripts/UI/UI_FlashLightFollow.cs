﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FlashLightFollow : MonoBehaviour
{

    
    RectTransform m_Transform;

    private void Awake()
    {
        m_Transform = GetComponent<RectTransform>();
    }

    void Update()
    {
        //Follow Mouse Position at Offset
        m_Transform.localPosition = new Vector2(Input.mousePosition.x,Input.mousePosition.y) - new Vector2(Screen.width/2,Screen.height/2);

    }
}
