﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UIManager : MonoBehaviour
{

    [SerializeField]
    GameObject[] m_UIMenus;


    [SerializeField]
    GameObject gameEnterButton;
    [SerializeField]
    GameObject gameList;

    List<GameObject> matchButtons = new List<GameObject>();
    public string gameName;

    int currentMenu;

    public NetworkManager manager;



    void Awake()
    {
        manager = GetComponent<NetworkManager>();

    }


    private void Start()
    {
        MenuCheck();
        enterMenu();
    }
    public void SetGameName(string nameToSet)
    {
        gameName = nameToSet;
    }

    public void enterMenu()
    {
        //Enter Designated Menu
        //0 = Main
        //1 - MatchMaking Main Area
        //2 - MatchMaking Found Matches
        //3 - Disconnect Screen
        for (int i = 0; i < m_UIMenus.Length; i++)
        {
            m_UIMenus[i].gameObject.SetActive(false);
        }

        m_UIMenus[currentMenu].gameObject.SetActive(true);

    }


    public void MenuCheck()
    {
        //Checks Network Manager for the appropiate current menu
        if (manager.matchMaker == null)
        {
            currentMenu = 0;
        }
        else if (manager.matchInfo == null)
        {
            if (manager.matches == null)
            {
                currentMenu = 1;
            }
            else
            {
                currentMenu = 2;
            }
            
        }

        enterMenu();
    }



    public void CreateGame()
    {
        manager.matchMaker.CreateMatch(gameName, manager.matchSize, true, "", "", "", 0, 0, manager.OnMatchCreate);
    }

    public void FindGame()
    {
        manager.matchMaker.ListMatches(0, 20, "", false, 0, 0, manager.OnMatchList);
    }

    public void MatchButtons()
    {
        //Generate and Clear Match buttons inside the match list
        matchButtons.Clear();

        for (int i = 0; i < manager.matches.Count; i++)
        {
            GameObject tempButton = Instantiate(gameEnterButton, gameList.transform);

            tempButton.GetComponent<ButtonPrefab>().DisplayText(manager.matches[i].name, i);

            matchButtons.Add(tempButton);

        }
        
    }

    public void JoinGame()
    {

    }
}
