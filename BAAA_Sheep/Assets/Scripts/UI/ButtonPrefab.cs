﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class ButtonPrefab : MonoBehaviour
{

    //Holds Found Matches inside scrollable viewing area

    NetworkManager manager;
    Text m_Text;
    int orderInArray;

    private void Awake()
    {
        m_Text = GetComponentInChildren<Text>();
        manager = GetComponentInParent<NetworkManager>();
    }

    //Update Text and Match ID
    public void DisplayText(string textToDisplay, int orderInRoomlist)
    {
        orderInArray = orderInRoomlist;
        m_Text.text = textToDisplay;
    }

    public void EnterMatch()
    {
        manager.matchMaker.JoinMatch(manager.matches[orderInArray].networkId, "", "", "", 0, 0, manager.OnMatchJoined);
    }

}
