﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingController : MonoBehaviour
{
    public bool isOn = false;
    GameObject visionPanel;

    [SerializeField]
    float rate;

    [SerializeField]
    float exposedTime;

    float timer;

    private void Awake()
    {
        timer = rate;
        visionPanel = transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        //Lighting timer
        if (timer <= Time.time && !visionPanel.activeSelf)
        {
            visionPanel.SetActive(true);
            StartCoroutine(deactivatePanel());
            timer = Time.time + rate + exposedTime;
        }
    }

    IEnumerator deactivatePanel()
    {
        //Disable lighting
        yield return new WaitForSeconds(exposedTime);
        visionPanel.SetActive(false);
    }
}
