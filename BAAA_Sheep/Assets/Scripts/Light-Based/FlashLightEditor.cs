﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(FlashLightCone))]
public class FlashLightEditor : Editor
{
    private void OnSceneGUI()
    {
        //Draw UI
        FlashLightCone flashLight = (FlashLightCone)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(flashLight.transform.position, Vector3.up, Vector3.forward, 360, flashLight.lightRadius);
        Vector3 viewAngleA = (flashLight.DirFromAngle(-flashLight.lightAngle / 2, false));
        Vector3 viewAngleB = (flashLight.DirFromAngle(flashLight.lightAngle / 2, false));
        Handles.DrawLine(flashLight.transform.position, flashLight.transform.position + viewAngleA * flashLight.lightRadius);
        Handles.DrawLine(flashLight.transform.position, flashLight.transform.position + viewAngleB * flashLight.lightRadius);

        
        foreach (var target in flashLight.inLightTargets)
        {
            Handles.color = Color.red;
            Handles.DrawLine(flashLight.transform.position, target.position);
        }
    }
}
#endif
