﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FlashLightCone : MonoBehaviour
{
    public float lightRadius = 8;
    [Range(0, 360)]
    public float lightAngle = 45;

    public float meshResolution = 0.8f;
    public int edgeResolveIterations = 4;
    public float edgeDisThreshold = 0.5f;
    private Light light;
    public MeshFilter lightMeshFilter;
    Mesh lightMesh;


    public LayerMask targetMask;
    public LayerMask obstacleMask;

    [HideInInspector]
    public List<Transform> inLightTargets = new List<Transform>();

    private void Start()
    {
        //Creare empty light mesh
        if (lightMeshFilter != null)
        {
            lightMesh = new Mesh();
            lightMesh.name = "Light Mesh";
            lightMeshFilter.mesh = lightMesh;
        }

        //Sync the light data
        light = GetComponent<Light>();
        if (light.type == LightType.Spot)
        {
            light.spotAngle = lightAngle;
        }
        light.range = lightRadius;

    }

    private void Update()
    {
        FindTargets();
    }

    private void LateUpdate()
    {
        DrawFlashLightField();
    }

    void FindTargets()
    {
        //Reset the recorded targets
        if (inLightTargets.Count != 0)
        {
            inLightTargets.Clear();
        }
        Collider[] targetInRadius = Physics.OverlapSphere(transform.position, lightRadius, targetMask);
        
        //Add any targets within Radius and Angles in the list
        for (int i = 0; i < targetInRadius.Length; i++)
        {
            Transform target = targetInRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < lightAngle / 2)
            {
                float disToTarget = Vector3.Distance(transform.position, target.position);

                if (!Physics.Raycast(transform.position, dirToTarget, disToTarget, obstacleMask))
                {
                    inLightTargets.Add(target);
                }
            }
        }
    }

    void DrawFlashLightField()
    {
        int stepCount = Mathf.RoundToInt(lightAngle * meshResolution);
        float stepAngleSize = lightAngle / stepCount;
        List<Vector3> lightPoints = new List<Vector3>();
        LightCastInfo oldLightCast = new LightCastInfo();
        //Cast Light rays
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = transform.eulerAngles.y - lightAngle / 2 + stepAngleSize * i;
            LightCastInfo newLightCast = lightCast(angle);
            if (i > 0)
            {
                bool edgeDisThresholdExceeded = Mathf.Abs(oldLightCast.distance - newLightCast.distance) > edgeDisThreshold;
                if (oldLightCast.hit != newLightCast.hit || (oldLightCast.hit && newLightCast.hit && edgeDisThresholdExceeded))
                {
                    //Find the edge of the obstacle 
                    EdgeInfo edge = findEdge(oldLightCast, newLightCast);
                    if (edge.pointA != Vector3.zero)
                    {
                        lightPoints.Add(edge.pointA);
                    }
                    if (edge.pointB != Vector3.zero)
                    {
                        lightPoints.Add(edge.pointB);
                    }
                }
            }
            lightPoints.Add(newLightCast.point);
            oldLightCast = newLightCast;
        }

        //Locate vertices and triangles
        int vertexCount = lightPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(lightPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        //Create Mash 
        lightMesh.Clear();
        lightMesh.vertices = vertices;
        lightMesh.triangles = triangles;
        lightMesh.RecalculateNormals();
    }

    //Return the cast info
    LightCastInfo lightCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, dir, out hit, lightRadius, obstacleMask))
        {
            return new LightCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new LightCastInfo(false, transform.position + dir * lightRadius, lightRadius, globalAngle);
        }
    }

    EdgeInfo findEdge(LightCastInfo minLightCast, LightCastInfo maxLightCast)
    {
        float minAngle = minLightCast.angle;
        float maxAngle = maxLightCast.angle;
        Vector3 minPoint = Vector3.zero;
        Vector3 maxPoint = Vector3.zero;

        //Find the middle point between hit obstacle light and none hit obstacle light
        for (int i = 0; i < edgeResolveIterations; i++)
        {
            float angle = (minAngle + maxAngle) / 2;
            LightCastInfo newLightCast = lightCast(angle);

            bool edgeDisThresholdExceeded = Mathf.Abs(minLightCast.distance - newLightCast.distance) > edgeDisThreshold;
            if (newLightCast.hit == minLightCast.hit && !edgeDisThresholdExceeded)
            {
                minAngle = angle;
                minPoint = newLightCast.point;
            }
            else
            {
                maxAngle = angle;
                maxPoint = newLightCast.point;
            }
        }

        return new EdgeInfo(minPoint, maxPoint);
    }

    //Light field angle
    public Vector3 DirFromAngle(float angleInDegree, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegree += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegree * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegree * Mathf.Deg2Rad));
    }

    public struct LightCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float distance;
        public float angle;

        public LightCastInfo(bool _hit, Vector3 _point, float _distance, float _angle)
        {
            hit = _hit;
            point = _point;
            distance = _distance;
            angle = _angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
        {
            pointA = _pointA;
            pointB = _pointB;
        }
    }
}
