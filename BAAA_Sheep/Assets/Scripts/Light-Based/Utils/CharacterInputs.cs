﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInputs
{
    public static Vector3 GetDirectionalInput()
    {
        return new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
    }
    public static bool wasCastSpellPressed()
    {
        return Input.GetKeyDown(KeyCode.K);
    }

    public static bool wasFirePressed()
    {
        return Input.GetKeyDown(KeyCode.J);
    }
    public static bool wasIcePressed()
    {
        return Input.GetKeyDown(KeyCode.I);
    }
    public static bool wasAirPressed()
    {
        return Input.GetKeyDown(KeyCode.P);
    }
    public static bool wasEarthPressed()
    {
        return Input.GetKeyDown(KeyCode.L);
    }

    public static bool isSelectorPressed()
    {
        return Input.GetKey(KeyCode.Space);
    }
    public static bool wasSelectorPressed()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }
}
